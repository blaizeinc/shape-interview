package com.zephr.interview;

import java.util.concurrent.CompletableFuture;

public interface Shape {

	CompletableFuture<Double> calculateArea();
}
